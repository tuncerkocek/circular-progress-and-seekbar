package com.tuncer.kocek.model;

/**
 * Créer par Tuncer Kocek  10/22/2018
 */
public class Circle {
    private double x;
    private double y;
    private double centerX;
    private double centerY;
    private double rayon;
    private double innerRayon;
    private double outerRayon;
    private double stroke;

    public Circle(double x, double y, double centerX, double centerY, double radius, double stroke) {
        this.x = x;
        this.y = y;
        this.centerX = centerX;
        this.centerY = centerY;
        this.stroke = stroke;
        setRadius(radius);

    }


    public void setRadius(double rayon) {
        this.rayon = rayon;
        this.innerRayon = rayon - stroke;
        this.outerRayon = rayon;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public double getRayon() {
        return rayon;
    }

    public double getInnerRayon() {
        return innerRayon;
    }

    public double getOuterRayon() {
        return outerRayon;
    }

    public double getStroke() {
        return stroke;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }

    public void setStroke(double stroke) {
        this.stroke = stroke;
    }

    /**
     * Change actuel circle at the new angle position
     *
     * @param intersectionCircle The intercept circle
     * @param endAngle           The angle where we need to put this circle
     * @param angleAdjust        The rotation of the intercept circle
     */
    public void alongCircleWithAngle(Circle intersectionCircle, double endAngle, double angleAdjust) {

        x = (intersectionCircle.getCenterX() + intersectionCircle.getOuterRayon() * (Math.cos(Math.toRadians(endAngle + angleAdjust)))); // +x because our view circle begin at x in place of 0
        y = intersectionCircle.getCenterY() + intersectionCircle.getOuterRayon() * (Math.sin(Math.toRadians(endAngle + angleAdjust))); // +x same than above
    }

    /**
     * Give the angle of the coordinate
     *
     * @param dx The x axis position that we want the angle
     * @param dy The y axis position that we want the angle
     * @return The angle that correspond to given position
     */
    public double getAngle(float dx, float dy) {
        return (Math.toDegrees(Math.atan2(dx - centerX, centerY - dy) + 360)) % 360;
    }

    /**
     * Find angle of x y position and put circle to the corresponded angle positin
     *
     * @param intersectionCircle Intercept circle
     * @param dx                 The x position where the click happend
     * @param dy                 THe y position where the click happend
     * @param semiCircle         If the intercept circle is a semiCircle(arc)
     * @return True If it's ok or not
     */
    public boolean alongCircleWithCoordinate(Circle intersectionCircle, float dx, float dy, boolean semiCircle) {
        double cos;
        double sin;
        if (semiCircle) {
            cos = Math.cos(Math.atan2(dx - intersectionCircle.getCenterX(), intersectionCircle.getCenterY() - dy) - (Math.PI / 2));
            sin = Math.sin(Math.atan2(dx - intersectionCircle.getCenterX(), intersectionCircle.getCenterY() - dy) - (Math.PI / 2));
            if (cos < (Math.sqrt(3) / 2) && cos > (-Math.sqrt(3)) / 2) {
                if (sin >= Math.sqrt(1) / 2) {
                    return false;
                }
            }
        } else {
            cos = Math.cos(Math.atan2(dx - intersectionCircle.getCenterX(), intersectionCircle.getCenterY() - dy) - (Math.PI / 2));
            sin = Math.sin(Math.atan2(dx - intersectionCircle.getCenterX(), intersectionCircle.getCenterY() - dy) - (Math.PI / 2));
        }

        x = (intersectionCircle.getCenterX() + intersectionCircle.getOuterRayon() * cos);
        y = (intersectionCircle.getCenterX() + intersectionCircle.getOuterRayon() * sin);
        return true;
    }

    /**
     * Verify if  the distance is in circle ?
     *
     * @param distance The distance to verify
     * @return True if it's in false otherwise
     */
    public boolean isBetweenInnerAndOuterRadius(float distance) {
        return distance < outerRayon + 90 && distance > innerRayon - 90;
    }
}
