package com.tuncer.kocek.library;

/**
 * Créer par Tuncer Kocek  10/22/2018
 */
public interface OnCircularChangeListener {
    void onChange(CircularView circularView, int newProgress);
}
