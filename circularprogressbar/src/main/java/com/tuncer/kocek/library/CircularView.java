package com.tuncer.kocek.library;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.example.circularprogressbar.R;

import com.tuncer.kocek.model.Circle;

public class CircularView extends View {
    /**
     * All color and style
     */
    private Paint paint;

    /**
     * The bound of a rectangle where to draw circle
     */
    private RectF boundOfCircle;

    /**
     * The sweep angle of the canvas.draw sweep angle parameter
     */
    private double endAngle = 0;
    /**
     * The circle stroke size
     */
    private int stroke = 15;
    /**
     * Max progress
     */
    private int maxProgress = 100;
    /**
     * Temp angle is the angle for 1 step of progress
     */
    private double tempAngle;
    /**
     * The current progress
     */
    private double progress = 0;

    /**
     * Canvas circle 180 degre permit to start at 9 o clock
     */
    private float startAngle = 180;

    /**
     * The progress circle
     */
    private Circle circle;
    /**
     * The circle indicator
     */
    private Circle circleIndicator;

    /**
     * The padding for circle
     */
    private int padding = 100;

    /**
     * The drawable for icon
     */
    private Drawable drawable;

    /**
     * If we show logo or not
     */
    private boolean showLogo = false;
    /**
     * The text color
     */
    private int textColor = getResources().getColor(R.color.blue);
    /**
     * The indicator circle color
     */
    private int indicatorCircleColor = getResources().getColor(R.color.blueDrak);
    /**
     * The color of the progress circle
     */
    private int progressCircleColor = getResources().getColor(R.color.blue);
    /**
     * The color of the background progress circle
     */
    private int backgroundProgressCircleColor = getResources().getColor(R.color.grey);
    /**
     * The size of text to show
     */
    private float textSize = getResources().getDimensionPixelSize(R.dimen.fontSize);
    /**
     * The bound of text
     */
    private Rect textBound = new Rect();
    /**
     * Is seekbar or progressbar ?
     */
    private boolean isSeekbar = true;
    /**
     * Listener for change
     */
    private OnCircularChangeListener onCircularChangeListener;
    /**
     * Text to draw
     */
    private String textToDraw;
    /**
     * If we need to show the indicator or not
     */
    private boolean showIndicatorCircle = true;
    /**
     * The string to put at the end of our textToDraw
     * Permit more customization
     */
    private String end = "";
    /**
     * If progress value to show  need to be updated after creation or not
     */
    private boolean toUpdate = false;
    /**
     * If user touch screen true or false if release his finger
     */
    private boolean screenIsTouched = false;
    /**
     * Do we need to show text or not
     */
    private boolean showText = true;
    /**
     * If the cercle is full or the custom 240 semi arc
     */
    private double maxAngle = 360;

    /**
     * Adjust angle in semi arc
     */
    private double adjustProgressCircle = 0;
    /**
     * rotate by this angle the trigonometric circle to begin at the same place that the progress circle
     */
    private int angleAdjust = 180;
    /**
     * The circle is the arc style or not
     */
    private boolean semiCircle = false;

    /**
     * The icon animation is set or not
     */
    private boolean alphaAnimation = true;


    /**
     * For programmatically initiation
     *
     * @param context The context
     */
    public CircularView(Context context) {
        super(context);

        init(context, null);
    }

    /**
     * For xml initiation
     *
     * @param context The context
     * @param attrs   The attribut that we need
     */
    private void init(Context context, AttributeSet attrs) {
        paint = new Paint();
        paint.setAntiAlias(true);
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(
                    attrs, R.styleable.CircularView);
            Drawable draw = array.getDrawable(R.styleable.CircularView_icon);
            if (draw != null) {
                drawable = draw;
            }

            stroke = array.getInteger(R.styleable.CircularView_strokeSize, 15);
            maxProgress = array.getInteger(R.styleable.CircularView_max, 100);
            showLogo = array.getBoolean(R.styleable.CircularView_showIcon, true);
            isSeekbar = array.getBoolean(R.styleable.CircularView_isSeekbar, true);
            showIndicatorCircle = array.getBoolean(R.styleable.CircularView_showSeekBarButton, true);
            showText = array.getBoolean(R.styleable.CircularView_showText, true);
            if (progress < 0 || progress > maxProgress) {
                throw new IllegalArgumentException("Progress can't be greater than Max progress or less than 0");
            }
            progressCircleColor = array.getColor(R.styleable.CircularView_progressCircleColor, getResources().getColor(R.color.blue));
            backgroundProgressCircleColor = array.getColor(R.styleable.CircularView_progressBackgroundCircleColor, getResources().getColor(R.color.grey));
            indicatorCircleColor = array.getColor(R.styleable.CircularView_indicatorColor, getResources().getColor(R.color.blueDrak));
            textColor = array.getColor(R.styleable.CircularView_textColor, getResources().getColor(R.color.blue));
            textSize = array.getDimensionPixelSize(R.styleable.CircularView_textSize, getResources().getDimensionPixelSize(R.dimen.fontSize));
            if (array.getInteger(R.styleable.CircularView_progress, 0) > maxProgress) {
                throw new IllegalArgumentException("Progress can't be greater than max progress");
            }
            setMaxProgress(array.getInteger(R.styleable.CircularView_progress, 0));
            array.recycle();
        }
        updateTextToDraw();
    }

    public CircularView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircularView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width;
        int height;

        if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        } else {
            width = 500;
        }
        if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY) {
            height = MeasureSpec.getSize(heightMeasureSpec);
        } else {
            height = 500;
        }

        if (width != height) {
            int min = Math.min(width, height);
            height = min;
            width = min;

        }
        boundOfCircle = new RectF(padding, padding, width - padding, height - padding);
        setMeasuredDimension(width, height);
        if (circle == null) {
            circle = new Circle(padding, (height / 2), ((width - (padding * 2)) / 2) + padding, ((height - (padding * 2)) / 2) + padding, ((width - (padding * 2)) / 2), stroke);
        }
        if (circleIndicator == null) {
            double rayon = (((width / 10) / 2)) / 2;
            circleIndicator = new Circle(padding, (height / 2), rayon, rayon, rayon, 0);
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawArc(canvas);
        drawIndicator(canvas);

        if (showLogo) {
            createDrawable(canvas);
            if (alphaAnimation) {
                drawable.setAlpha((int) Math.round((progress / maxProgress) * 255));
            } else {
                drawable.setAlpha(255);
            }

            drawable.draw(canvas);
        } else if (showText) {
            drawText(canvas);
        }
    }

    /**
     * Draw the text a the screen
     *
     * @param canvas The canvas where to draw
     */
    private void drawText(Canvas canvas) {
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(8);
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.getTextBounds(textToDraw, 0, textToDraw.length(), textBound);
        while (textBound.width() >= circle.getInnerRayon()) {
            textSize--;
            paint.setTextSize(textSize);
            paint.getTextBounds(textToDraw, 0, textToDraw.length(), textBound);
        }
        canvas.drawText(textToDraw, (int) circle.getCenterX(), (int) circle.getCenterY() + (textBound.height() / 2), paint);
    }

    /**
     * Draw our icon in center of circle
     *
     * @param canvas the canvas where to draw
     */
    private void createDrawable(Canvas canvas) {
        if (drawable == null) {
            drawable = getResources().getDrawable(R.drawable.ic_android_black_24dp);
        }
        drawable.setBounds(padding * 2, padding * 2, getWidth() - (padding * 2), getHeight() - (padding * 2));
    }

    /**
     * Draw the indicator
     *
     * @param canvas The canvas where to draw
     */
    private void drawIndicator(Canvas canvas) {
        if (toUpdate) {
            circleIndicator.alongCircleWithAngle(circle, endAngle, angleAdjust); // update position of indicator if setprogress was call before creating the view
            toUpdate = false;
        }
        if (showIndicatorCircle && isSeekbar) {
            paint.setStyle(Paint.Style.FILL);
            if (screenIsTouched) { //If the user leave his finger on screen
                paint.setColor(Color.BLACK);
                paint.setAlpha(155);
                canvas.drawCircle((int) circleIndicator.getX(), ((int) circleIndicator.getY()), (float) (circleIndicator.getRayon() * 2) + 25, paint); // Create the background of indicator to simulate a visible touched effect
            }
            paint.setColor(indicatorCircleColor);
            canvas.drawCircle((int) circleIndicator.getX(), ((int) circleIndicator.getY()), (float) (circleIndicator.getRayon() * 2), paint); //Draw the real indicator
        }
    }

    /**
     * Draw all arc
     *
     * @param canvas The canvas where to draw
     */
    private void drawArc(Canvas canvas) {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(stroke);
        paint.setColor(backgroundProgressCircleColor);
        canvas.drawArc(boundOfCircle, startAngle, (float) maxAngle, false, paint);//The background progress circle
        paint.setColor(progressCircleColor);
        canvas.drawArc(boundOfCircle, startAngle, (float) endAngle, false, paint); //The progress circle
    }


    /**
     * Set the arc style or not
     *
     * @param arcStyle True if the view need to be arc style , false if not
     */
    public void setSemiArcStyle(boolean arcStyle) {
        if (arcStyle) {
            initArcStyle();
        } else {
            resetDefaultView();
        }

        invalidate();
    }

    private void resetDefaultView() {
        maxAngle = 360; //Default circle view
        startAngle = 180; //We begin circle at 9 o clock
        angleAdjust = 180; // We rotate the circle by 180 to fit at 9 o clock
        semiCircle = false; //Is not a semi circle
        adjustProgressCircle = 0; //We doesn't need to adapt the indicator because it's a normal circle
        endAngle = 0; //Reset the progress line
        setProgress(0); // // Put progress to 0
    }

    private void initArcStyle() {
        maxAngle = 240; //Arc style need to be an arc of 240 degrees
        startAngle = 150; //We start at 7 o clock
        angleAdjust = 150; //We rotate the circle by 150 degree to fit the 7 o clock
        semiCircle = true; //It's a semi circle
        endAngle = 0; //Reset progress line
        adjustProgressCircle = 30; //substract the line by 30 degree to fo fit at 150 o clock
        setProgress(0); //Reset progress on change style
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                screenIsTouched = true; //True if screen is touched
                break;
            case MotionEvent.ACTION_UP:
                screenIsTouched = false; //False if screen is released
        }
        moved(event.getX(), event.getY());
        return true;
    }

    /**
     * Move the indicator and line to x-y position if it's at the border of circle
     *
     * @param x The x axis where to put indicator and line
     * @param y The y ais where to put indicator and line
     */
    private void moved(float x, float y) {
        if (isSeekbar) {
            float distance = (float) Math.sqrt(Math.pow((x - circle.getCenterX()), 2) + Math.pow((y - circle.getCenterY()), 2));
            if (circle.isBetweenInnerAndOuterRadius(distance)) {
                if (circleIndicator.alongCircleWithCoordinate(circle, x, y, semiCircle) || !showIndicatorCircle) {
                    updateView(circle.getAngle(x, y), true);
                    onChange();

                }

            }
        }
        invalidate();
    }

    /**
     * Give the current progress
     *
     * @return
     */
    public double getProgress() {
        return progress;
    }

    /**
     * Update the endAngle, progress with new information
     *
     * @param angle  The new angle where to put indicator and line
     * @param update true if we need to update progress with angle or false if it's already update
     */
    private void updateView(double angle, boolean update) {
        getTempAngle();
        if (semiCircle) {
            if (circleIndicator != null)
                angle += (circleIndicator.getOuterRayon() * 2) - 10;

            if (angle > maxAngle) {
                angle -= 360;
                if (angle < 0) {
                    angle = Math.abs(angle);
                }
            }
        } else {
            if (circleIndicator != null && update) {
                angle -= circleIndicator.getOuterRayon();
            }
            if (angle < 0) {
                angle += 360;
            }
        }
        if (!toUpdate && update) {
            progress = Math.round(angle / tempAngle);
        }

        endAngle = tempAngle * progress;
        updateTextToDraw();

    }

    /**
     * Return angle
     */
    private void getTempAngle() {
        tempAngle = maxAngle / maxProgress;
    }

    /**
     * Show logo or not
     *
     * @param showLogo True if logo must be showed false otherwise
     */
    public void showLogo(boolean showLogo) {
        this.showLogo = showLogo;
        invalidate();
    }


    /**
     * Set stroke size
     *
     * @param stroke The new stroke size
     */
    public void setStrokeSize(int stroke) {
        this.stroke = stroke;
        invalidate();
    }

    /**
     * Set the new progress
     *
     * @param progress The new progress value
     */
    public void setProgress(double progress) {
        if (progress > maxProgress) {
            this.progress = maxProgress;
        } else {
            this.progress = progress;
        }
        updateProgressView();

        invalidate();
        onChange();


    }

    /**
     * Call the listener if not null with new progress
     */
    private void onChange() {
        if (onCircularChangeListener != null) {
            onCircularChangeListener.onChange(this, (int) this.progress);
        }
    }

    /**
     * Update progress line
     */
    private void updateProgressView() {
        if (circleIndicator != null && !semiCircle) {
            getTempAngle();
            updateView(tempAngle * progress * adjustProgressCircle, false);
            circleIndicator.alongCircleWithAngle(circle, endAngle, startAngle);

        } else {
            toUpdate = true;
            getTempAngle();
            updateView(tempAngle * progress * adjustProgressCircle, false);
        }

    }

    /**
     * Update the text
     */
    private void updateTextToDraw() {
        if (isSeekbar) {
            textToDraw = (int) progress + end;
        } else {
            textToDraw = (int) progress + "";
            if (maxProgress == 100) {
                textToDraw += "%";
            }
        }

    }

    /**
     * Set the new drawable
     * @param drawable The new drawable
     */
    public void setDrawable(Drawable drawable) {
        if(drawable==null){
            throw new NullPointerException("Drawable can't be null");
        }
        this.drawable = drawable;
        invalidate();
    }

    /**
     * Set the new text color
     * @param textColor The new color
     */
    public void setTextColor(int textColor) {
        this.textColor = textColor;
        invalidate();
    }

    /**
     * Set new color to indicator
     * @param indicatorCircleColor The new color
     */
    public void setIndicatorCircleColor(int indicatorCircleColor) {
        this.indicatorCircleColor = indicatorCircleColor;
        invalidate();
    }

    /**
     * Set new color to progress circle
     * @param progressCircleColor The new color
     */
    public void setProgressCircleColor(int progressCircleColor) {
        this.progressCircleColor = progressCircleColor;
        invalidate();
    }

    /**
     * Set new color to background progress circle
     * @param backgroundProgressCircleColor The new color
     */
    public void setBackgroundProgressCircleColor(int backgroundProgressCircleColor) {
        this.backgroundProgressCircleColor = backgroundProgressCircleColor;
        invalidate();
    }

    /**
     * Set new size to text
     * @param textSize The new size
     */
    public void setTextSize(float textSize) {
        this.textSize = textSize;
        invalidate();
    }

    /**
     * Set seekbar style or progressbar
     * @param seekbar True if view is seekbar false if it's progress bar
     */
    public void setSeekbar(boolean seekbar) {
        isSeekbar = seekbar;
        invalidate();
    }

    /**
     * Set new Listener
     * @param onCircularChangeListener The new Listener
     */
    public void setOnCircularChangeListener(OnCircularChangeListener onCircularChangeListener) {
        if(onCircularChangeListener==null){
            throw new NullPointerException("Listener can't be null");
        }
        this.onCircularChangeListener = onCircularChangeListener;
    }

    /**
     * Set text to end by example we can show % at the of value when the view is progress bar
     * @param text The text to show at the end of progress value
     */
    public void setTextToEnd(String text) {
        end = text;
        invalidate();
    }

    /**
     * Get the max progress value
     * @return The max progress
     */
    public int getMaxProgress() {
        return maxProgress;
    }

    /**
     * Set new value to max progress
     * @param maxProgress The new max value
     */
    public void setMaxProgress(int maxProgress) {

        if (maxProgress == 0) {
            this.maxProgress = 100;
        } else {
            this.maxProgress = maxProgress;
        }
        setProgress(0);
        updateTextToDraw();
        invalidate();
    }

    /**
     * Show or not the seekbar indicator
     * @param showSeekbarButton True if it's need to be showed false otherwise
     */
    public void showSeekbarButton(boolean showSeekbarButton) {
        if (!isSeekbar) {
            throw new IllegalArgumentException("Seekbar button can only be showed in seekbar mode");
        }
        this.showIndicatorCircle = showSeekbarButton;
        invalidate();
        invalidate();

    }

    /**
     * Show the text or not
     * @param showText True if the text need to be showed false otherwise
     */
    public void showText(boolean showText) {
        this.showText = showText;
        invalidate();
    }

    /**
     * Show animation or not
     * @param alphaAnimation True if yes false otherwise
     */
    public void setAlphaAnimation(boolean alphaAnimation) {
        this.alphaAnimation = alphaAnimation;
        invalidate();
    }


}
