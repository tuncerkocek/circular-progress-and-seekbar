package com.tuncer.sample;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.tuncer.kocek.library.CircularView;
import com.tuncer.kocek.library.OnCircularChangeListener;
import com.example.tuncer.sample.R;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private CircularView circularView;
    private boolean launched;
    private Timer tim = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        circularView = findViewById(R.id.progress_circular);
        final CheckBox arcStyle = findViewById(R.id.arcStyle);
        arcStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel(tim);
                circularView.setSemiArcStyle(arcStyle.isChecked());
            }
        });
        final CheckBox seekBar = findViewById(R.id.seekBar);
        seekBar.setChecked(true);
        seekBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                circularView.setSeekbar(seekBar.isChecked());
            }
        });
        final CheckBox seekBarButton = findViewById(R.id.seekBarButton);
        seekBarButton.setChecked(true);
        seekBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                circularView.showSeekbarButton(seekBarButton.isChecked());
            }
        });
        final CheckBox showText = findViewById(R.id.textShow);
        final CheckBox showLogo = findViewById(R.id.logo);
        showLogo.setChecked(true);
        showText.setChecked(true);

        showText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showLogo.isChecked()) {
                    snackBarShow("Text doesn't show when show logo is activated");
                }
                circularView.showText(showText.isChecked());
            }


        });

        showLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                circularView.showLogo(showLogo.isChecked());
            }
        });

        final CheckBox animate = findViewById(R.id.animation);
        animate.setChecked(true);
        animate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!showLogo.isChecked()) {
                    Snackbar snackbar = Snackbar
                            .make(circularView, "Show logo first", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                circularView.setAlphaAnimation(animate.isChecked());
            }
        });
        EditText progress = findViewById(R.id.progress);
        progress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                circularView.setProgress(Integer.valueOf(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText progressMax = findViewById(R.id.progressMax);
        progressMax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                circularView.setMaxProgress(Integer.valueOf(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        EditText textColor = findViewById(R.id.textColor);
        textColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                try {
                    circularView.setTextColor(Color.parseColor((s.toString())));
                } catch (Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        EditText backgroundColor = findViewById(R.id.backgroundColor);
        backgroundColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                try {
                    circularView.setBackgroundProgressCircleColor(Color.parseColor(s.toString()));
                } catch (Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText progressColor = findViewById(R.id.progressColor);
        progressColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                try {
                    circularView.setProgressCircleColor(Color.parseColor(s.toString()));
                } catch (Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText seekBarButtonColor = findViewById(R.id.seekBarButtonColor);
        seekBarButtonColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                try {
                    circularView.setIndicatorCircleColor(Color.parseColor(s.toString()));
                } catch (Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText strokeSize = findViewById(R.id.strokeSize);
        strokeSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                circularView.setStrokeSize(Integer.valueOf(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText textSize = findViewById(R.id.textSize);
        textSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) return;
                circularView.setTextSize(Integer.valueOf(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText textToEnd = findViewById(R.id.textToEnd);
        textToEnd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                circularView.setTextToEnd(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                circularView.setProgress(circularView.getProgress() + 1);
            }
        };

        circularView.setOnCircularChangeListener(new OnCircularChangeListener() {
            @Override
            public void onChange(CircularView circularView, int newProgress) {
                snackBarShow(String.valueOf(newProgress));
                if (circularView.getProgress() == circularView.getMaxProgress()) {
                    tim.cancel();
                    launched = false;
                }
            }
        });
        Button go = findViewById(R.id.test);
        launched = false;
        circularView.setProgress(7);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!launched) {
                    circularView.setProgress(0);
                    tim=new Timer();
                    tim.schedule(task, 1000, 1000);
                    launched = true;
                } else {
                    snackBarShow("Already started");
                }


            }
        });
    }

    private void cancel(Timer tim) {
        if (launched) {
            tim.cancel();
            launched = false;
        }
    }

    private void snackBarShow(String mess) {
        Snackbar snackbar = Snackbar
                .make(circularView, mess, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}

