# CircularView
[Apache2](https://www.apache.org/licenses/LICENSE-2.0)

**CircularView** - An Android custom circular SeekBar convertable to Progress bar that supports max range, alpha icon animation and arc style.
<p align="center"><img src="Screen/1.png" /></p>

## Gradle

```java

UNDER BUILD.GRADLE(PROJECT)
In :

allprojects {
    repositories {
        google()
        jcenter()
    }
}
add following link:
        maven {
            url  "https://dl.bintray.com/tuncerkocek/CircularView"
        }
        
        so it will looks like after the adding
        allprojects {
            repositories {
                google()
                jcenter()
                maven {
                    url  "https://dl.bintray.com/tuncerkocek/CircularView"
                }
        }
        
Then in app build.gradlle 

dependencies {
    implementation 'com.tuncer.kocek:circularprogressbar:0.1.1@aar'

}
```

## Usage

* In XML layout: 

```xml
 <com.tuncer.kocek.library.CircularView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            />
            
            **Remember** Don't add padding, the widget is already centered. Use layout gravity center to center it in layout or margin !
* In JAVA : 

```Java
CircularView circularView=new CircularView(this) //this is the context! For fragment do getContext()

# Visual style Customization
circularView.setSemiArcStyle(true); // change the style to arc style
circularView.setSeekbar(true); //set the style to seekbar! With false, the widget become an untouchable progressbar. Perfect for loading
circularView.showSeekbarButton(true); //Show the indicator
circularView.showText(true); //Show the text
circularView.showLogo(true); //Show the logo
circularView.setAlphaAnimation(true); //Set opacity animation to icon

#Value customization
circularView.setProgress(100); //Change current progress to 100
circularView.setMaxProgress(150); // Change max progress value to 150

# Color style Customization 
 circularView.setTextColor(Color.parseColor("#000000")); //Change the text color to black
 or
 circularView.setTextColor(Color.BLACK); //Change the text color to black
 
 
 circularView.setBackgroundProgressCircleColor(Color.parseColor("#000000")); //Change background progress circle to black
 or 
 circularView.setBackgroundProgressCircleColor(Color.BLACK); //Change background progress circle to black
  
 circularView.setProgressCircleColor(Color.parseColor("#000000"));  //Change progress circle to black
 or 
  circularView.setProgressCircleColor(Color.BLACK);//Change progress circle to black
  
  circularView.setIndicatorCircleColor(Color.parseColor("#000000")); //Change indicator color to black
  or
   circularView.setIndicatorCircleColor(Color.BLACK); //Change indicator color to black
   
# Text Style and size Customization  
   circularView.setStrokeSize(15); //Change progress circle stroke to 15
   
    circularView.setTextSize(15); //Set text size to 15
    
    circularView.setTextToEnd("%"); //Put % at the end of the showing text
    
```



* All customizable attributes:

```xml
<declare-styleable name="CircularView">
        <attr name="textColor" format="color" />  //Change the text color
        <attr name="textSize" format="dimension" /> //Change the text size
        <attr name="max" format="integer" /> // change the max value
        <attr name="progress" format="integer" /> //change current progress
        <attr name="icon" format="reference" /> //Change the centered icon
        <attr name="showIcon" format="boolean" /> //Show icon  If false it show Text
        <attr name="indicatorColor" format="color" /> //Change the indicator circle color
        <attr name="progressCircleColor" format="color" /> //Change the progress circle color
        <attr name="progressBackgroundCircleColor" format="color" /> //Change the progress background circle color
        <attr name="isSeekbar" format="boolean" /> //Is a seekbar or progressbar ? True it is, false if it's a progress bar Progress bar can't be touched by user
        <attr name="strokeSize" format="integer" /> //The stroke size of progress background circle and progress circle
        <attr name="showSeekBarButton" format="boolean" /> //Show the seekbar indicator or not
        <attr name="showText" format="boolean" /> //Show text or not The text would be shown only if show logo is false
    </declare-styleable>
```

## Sample
* Clone the repository and check out the `app` module.

## Licence
Copyright 2018 Tuncer Kocek

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.